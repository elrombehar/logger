var config = require('config.json')('logger.' + process.env.NODE_ENV + '.config.json');
var colors = require('colors');

var enableColors = true;
var logOrigin = '';

function getTime()
{
  var now = new Date();
  return now.toJSON();
}

function ConsoleLogger(origin, cb) {
  try {
    if(!origin)logOrigin = '';
    else logOrigin = '[' + origin + ']';
    enableColors = config.console.enableColors;
    this.write('internal','ConsoleLogger initaited for ');
    return cb(null);
  }
  catch(e){
    return cb(e);
  }
}

ConsoleLogger.prototype.write = function(mode, message)
{
 var msg = getTime() + ' [' + mode +'] ' + message + ' ' + logOrigin;

  if(enableColors)
  {
    switch(mode)
    {
      case 'success':
          console.log(msg.green);
          break;
      case 'trace':
          console.log(msg.magenta);
          break;
      case 'debug':
          console.log(msg.blue);
          break;
      case 'info':
          console.log(msg.white);
          break;
      case 'warn':
          console.log(msg.yellow);
          break;
      case 'error':
          console.log(msg.red);
          break;
      case 'fatal':
          console.log(msg.red);
          break;
      case 'internal':
          console.log(msg.bgBlue.white);
          break;
    }
  }
  else console.log(msg);
};

module.exports = ConsoleLogger;

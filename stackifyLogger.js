var config = require('config.json')('logger.' + process.env.NODE_ENV + '.config.json');
var ConsoleLogger = require('./consoleLogger.js');
var stackify = require('stackify-logger');

var consolelogger;

function StackifyLogger(clogger, origin, cb) {
  consolelogger = clogger;
  // Init stackify
  try {
    stackify.start({apiKey: config.stackify.apiKey, env: config.stackify.env});
    consolelogger.write('internal','Stackify initiated for');
    return cb(null);
  }
  catch(e) {
    return cb(e);
  }
}

StackifyLogger.prototype.write = function(mode, message, cb) {
  if(mode === 'success') mode = 'info';
    if(mode === 'fatal') mode = 'error';
      if(mode === 'internal') mode = 'info';
  try {
    if(mode === 'error') {
      stackify.log(mode, {error : new Error(message)});
    }
    else stackify.log(mode,message);
    return cb(null);
  }
  catch(e) {
    return cb(e);
  }
};

module.exports = StackifyLogger;

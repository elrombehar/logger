var config = require('config.json')('logger.' + process.env.NODE_ENV + '.config.json');
var fs = require('fs-extra');
var async = require('async');
var locks = require('locks');
var path = require('path');
var os = require("os");


var consolelogger;
var lastDateSet = '';
var currentLogFile = '';
var basePath = '';
var writeMutex;
var replaceFileCheckCounter = 0;

function getTime()
{
  var now = new Date();
  return now.toJSON();
}

function getDate()
{
  var toReturn = '';
  var now = new Date();
  var day = now.getDate();
    if(day < 10)day = '0' + day;
  var month = now.getMonth();
    if(month < 10)month = '0' + month;
  var year = now.getFullYear();
  toReturn = day + '-' + month + '-' + year;
  return toReturn;
}

function replaceFile()
{
  if(replaceFileCheckCounter == 10) {
    consolelogger.write('internal','Checking for file replacement...');
    replaceFileCheckCounter = 0;
  } else {
    replaceFileCheckCounter += 1;
  }
    if(getDate() != lastDateSet)
    {
      lastDateSet = getDate();
      var logPath = path.resolve(basePath,config.writeToFile.logDirectory);
      currentLogFile = path.resolve(logPath,config.writeToFile.logFilePrefix + getDate() + '.log');
      writeToFile('Log started for ' + lastDateSet, false, function(err){
        if(err)consolelogger.write('error',err.message + ' ' + err.stack);
        else consolelogger.write('internal','New log file created');
      });
  }
}

function writeToFile(logmessage,append,cb)
{
  writeMutex.timedLock(5000, function (merr) {
    if(merr)return cb(merr);
    else {
      if(currentLogFile !== '') {
        if(append) {
          fs.appendFileSync(currentLogFile, logmessage + os.EOL, 'utf8');
          writeMutex.unlock();
          return cb(null);
          /*
          fs.appendFile(currentLogFile, logmessage + os.EOL, 'utf8', function (err) {
            writeMutex.unlock();
            return cb(err);
          });
          */
        }
        else {
          fs.writeFile(currentLogFile,logmessage + os.EOL, 'utf8');
          writeMutex.unlock();
          return cb(null);
        }
      }
      else {
        writeMutex.unlock();
        return cb(new Error('currentLogFile invalid ' + currentLogFile));
      }
    }
  });
}

function FileLogger(clogger, origin, basepath,cb) {
  consolelogger = clogger;
  basePath = basepath;
  writeMutex = locks.createMutex();
  var logPath = path.resolve(basePath,config.writeToFile.logDirectory);

  try { fs.mkdirsSync(logPath); }
  catch (e) { return cb(e); }
  finally {}

  consolelogger.write('internal','log Path ' + logPath + ' created');
  currentLogFile = path.resolve(logPath,config.writeToFile.logFilePrefix + getDate() + '.log');
  lastDateSet = getDate();
  writeToFile('Log started for ' + lastDateSet, false, function(err){
    if(err) {
      consolelogger.write('error',err.message + ' ' + err.stack);
      return cb(err);
    }
    else consolelogger.write('internal','FileLogger initiated for');
    // Timer for checking if the log file needs to be changed
    setInterval(function(){
      replaceFile();
    }, 60 * 1000);
    return cb(err);
  });
}

FileLogger.prototype.write = function(mode,message,cb)
{
  var msg = getTime() + ' [' + mode + '] ' + message;
  writeToFile(msg, true, function(err){
    return cb(err);
  });
};

module.exports = FileLogger;

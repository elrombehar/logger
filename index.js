var config = require('config.json')('logger.' + process.env.NODE_ENV + '.config.json');
var StackifyLogger = require('./stackifyLogger.js');
var ConsoleLogger = require('./consoleLogger.js');
var SmsSender = require('./smsSender.js');
var FileLogger = require('./fileLogger.js');
var EmailSender = require('./emailSender.js');
var waterfall = require('async-waterfall');
var colors = require('colors');
var async = require('async');

var logOrigin = '';
var consolelogger;
var stackifyEnabled = false;
var stackifylogger;
var writeToFile = false;
var fileLogger;
var smsSender;
var sendSms = false;
var emailSender;
var sendEmail = false;

function parseArgs(args)
{
  var toReturn = '';
  for (var i = 1; i < args.length; i++) {
    toReturn += args[i] + ' ';
  }
  return toReturn;
}

function Logger(origin,basepath/*,cb*/) {

  logOrigin = origin;

  console.log('Logger running with ' + process.env.NODE_ENV.green + ' configuration');

  async.parallel([
    function(callback){
      consolelogger = new ConsoleLogger(origin, function(err){
        if(err)callback(err);
        else callback(null,'Console logger initiated');
      });
    },
    function(callback){
      if(config.stackify.enabled) {
        stackifylogger = new StackifyLogger(consolelogger, origin, function(err){
          if(err) {
            consolelogger.write('warn','Unable to initialize stackify logger : ' + err.message);
            callback(err);
          }
          else {
            stackifyEnabled = true;
            callback(null,'Stackify logger initiated');
          }
        });
      }
      else callback(null);
    },
    function(callback){
      if(config.writeToFile.enabled)
      {
        fileLogger = new FileLogger(consolelogger, origin,basepath, function(err){
          if(err) {
            consolelogger.write('warn','Unable to initialize file logger : ' + err.message);
            callback(err);
          }
          else  {
            writeToFile = true;
            callback(null,'File logger initiated');
          }
        });
        //fileLogger = new FileLogger(consolelogger, origin,basepath,null);
        //callback(null,'File logger initiated');
      }
      else callback(null);
    },
    function(callback){
      if(config.sendSms.enabled)
      {
        smsSender = new SmsSender(consolelogger, origin, function(err){
          if(err) {
            consolelogger.write('warn','Unable to initialize send sms : ' + err.message);
            callback(err);
          }
          else {
            sendSms = true;
            callback(null,'Sms sender initiated');
          }
        });
      }
      else callback(null);
    },
    function(callback){
      if(config.sendEmail.enabled)
      {
        emailSender = new EmailSender(consolelogger, origin, function(err){
          if(err){
            consolelogger.write('warn','Unable to initialize send email : ' + err.message);
            callback(err);
          }
          else {
            sendEmail = true;
            callback(null,'Email sender initiated');
          }
        });
      }
      else callback(null);
    },
    function(callback){
      callback(null,'Looger Init successful');
    }
  ], function(err, result){
    if(err)console.log('internal',JSON.stringify(err));
    //console.log(result);
    return;
  });
}

Logger.prototype.add = function() {
  var mode;
  if(arguments.length === 0)return;
    if(arguments.length === 1) {
      mode = 'info';
    }
    else {
      mode = arguments[0];
    }

  var message = parseArgs(arguments);
  if(config.types[mode].console) consolelogger.write(mode,message);
    if(stackifyEnabled && config.types[mode].stackify)stackifylogger.write(mode,message, function(err){
      if(err)consolelogger.write('internal','Failed to log at stackify ' + JSON.stringify(err));
    });
      if(writeToFile && config.types[mode].file)fileLogger.write(mode,message,function(err){
        if(err)consolelogger.write('internal','Failed to write to file ' + JSON.stringify(err));
        else consolelogger.write('internal','Wrote to file');
      });
        if(sendSms && config.types[mode].sms)smsSender.sendSms(mode, function(err,result){
          if(err)consolelogger.write('internal','Failed to send sms ' + JSON.stringify(err) + ' ' + JSON.stringify(result));
          else consolelogger.write('internal','Internal sms sent');
        });
        if(sendEmail && config.types[mode].email)emailSender.sendEmail('internal',message, function(err,result){
            if(err)consolelogger.write('internal','Failed to send email ' + JSON.stringify(err) + ' ' + JSON.stringify(result));
            else consolelogger.write('internal','Internal email sent ');
        });
};

module.exports = Logger;

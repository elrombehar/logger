var config = require('config.json')('logger.' + process.env.NODE_ENV + '.config.json');
var Nexmo = require('nexmo');

var nexmo;
var logOrigin = '';
var consolelogger;

function SmsSender(clogger, origin, cb) {
  try {
    logOrigin = origin;
    consolelogger = clogger;
    nexmo = new Nexmo( { apiKey : config.sendSms.nexmo.apiKey , apiSecret : config.sendSms.nexmo.apiSecret});
    consolelogger.write('internal','SmsSender initiated for');
    return cb(null);
  }
  catch(e)
  {
    return cb(e);
  }
}

SmsSender.prototype.sendSms = function(message, cb) {
  if(nexmo !== null) {
    nexmo.message.sendSms(config.sendSms.nexmo.fromNumber,config.sendSms.nexmo.toNumber,'[' + process.env.NODE_ENV + '][' + logOrigin + ']'+ message,function(err,result){
      cb(err,result);
    });
  }
  else return cb(new Error('Unable to send sms message ' + message + ' to ' + tonumber + '. nexmo object is null.',null));
};

module.exports = SmsSender;

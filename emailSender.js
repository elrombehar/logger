var config = require('config.json')('logger.' + process.env.NODE_ENV + '.config.json');
var mandrillTransport = require('nodemailer-mandrill-transport');
var nodemailer = require("nodemailer");

var logOrigin = '';
var consolelogger;
var mailTransport;

function EmailSender(clogger, origin, cb) {
  logOrigin = origin;
  consolelogger = clogger;
  process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"; // ignore self certificate error
  if(config.sendEmail.service.name.toLowerCase() === 'gmail') {
    mailTransport = nodemailer.createTransport('smtps://' + config.sendEmail.service.user + ':' + config.sendEmail.service.password + '@smtp.gmail.com');
  }
  else if (config.sendEmail.service.name.toLowerCase() === 'mandrill') {
    mailTransport = nodemailer.createTransport(mandrillTransport({
      auth: {
        apiKey: config.sendEmail.service.apiKey
      }
    }));
  }
  else {
    return cb(new Error('No valid service is defined for sending emails.'));
  }
  if(mailTransport === null)return cb(new Error('Unable to create mail transport'));
  else  {
    consolelogger.write('internal','emailSender initiated for');
    return cb(null);
  }
}


EmailSender.prototype.sendEmail = function(emailsubject,emailtext, cb) {
  if(mailTransport !== null) {
    mailTransport.sendMail({
      from: config.sendEmail.from,
      to: config.sendEmail.to,
      subject: '[' + process.env.NODE_ENV + '][' + logOrigin + ']' + emailsubject,
      text: emailtext
    }, function(error, response){
      cb(error,response);
    });
  }
  else return cb(new Error('Unable to send email message ' + subject + '. mailTransport object is null.',null));
};

module.exports = EmailSender;
